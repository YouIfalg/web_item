from flask import Flask, session
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect
from redis import StrictRedis

'''
 1.配置数据库
 2.配置redis
 3.配置数据库迁移
'''

app = Flask(__name__)

class Config(object):
    """工程配置信息"""
    SECRET_KEY = "EjpNVSNQTyGi1VvWECj9TvC/+kq3oujee2kTfQUs8yCM6xX9Yjq52v54g+HVoknA"

    DEBUG = True

    # 数据库的配置信息
    SQLALCHEMY_DATABASE_URI = "mysql://root:123@127.0.0.1:3306/information"
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # 关闭数据跟踪

    # redis配置
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379

    # flask_session的配置信息
    SESSION_TYPE = "redis"  # 指定 session 保存到 redis 中
    SESSION_USE_SIGNER = True  # 让 cookie 中的 session_id 被加密签名处理
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)  # 使用 redis 的实例
    SESSION_PERMANENT = False  # 设置session需要过期
    PERMANENT_SESSION_LIFETIME = 86400 * 2  # session 的有效期，单位是秒

app.config.from_object(Config) # 加载配置项
db = SQLAlchemy(app) # 初始化数据库

redis_store = StrictRedis(host=Config.REDIS_HOST, port=Config.REDIS_PORT)

# 开启csrf保护
CSRFProtect(app)
Session(app)# 初始化session数据

manager = Manager(app) # 创建终端命令对象
Migrate(app, db) # 将app与db关联
manager.add_command('db', MigrateCommand) # 添加迁移命令到manay中

@app.route('/')
def index():
    session['name']="itheima"
    return 'index'

if __name__ == '__main__':
    """已添加命令行,运行在命令行或者在环境中添加runserver"""
    manager.run()

